﻿using DotNetNuke.Entities.Users;
using MBZMajlis.dbContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MBZMajlis.Service
{
    public class DigestService
    {
        private DNNContext dbContext = new DNNContext();

        public int GetUserId()
        {
            var row = dbContext.Digest.FirstOrDefault();
            return row.UserId.Value;
        }

        public void UpdateUserId(int userId)
        {
            var ent = dbContext.Digest.FirstOrDefault();
            ent.UserId = userId;
            dbContext.Digest.Attach(ent);
            dbContext.Entry(ent).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
               

    }
}