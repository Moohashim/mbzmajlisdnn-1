﻿using DotNetNuke.Entities.Users;
using MBZMajlis.Config;
using MBZMajlis.Controllers.dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace MBZMajlis.Service
{
    public static class EmailService
    {
        public static string EmailTemplatesPath = "EmailTemplates";

        private static void SendEmailBySmtp(MailMessage mail)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient(ConfigSettings.smtpHost);

                SmtpServer.EnableSsl = ConfigSettings.smtpEnableSsl;
                if (SmtpServer.EnableSsl)
                    SmtpServer.Port = ConfigSettings.smtpSSLPort;
                else
                    SmtpServer.Port = ConfigSettings.smtpNoSSLPort;

                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigSettings.smtpUserName, ConfigSettings.smtpPassw);

                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void SendEmailAdminRegistrationDigest(List<string> emails)
        {

            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(ConfigSettings.emailFrom, ConfigSettings.emailFromName);
            mail.To.Add(ConfigSettings.emailTo);

            mail.Subject = string.Format("New 100 users are registered");

            StringBuilder body = new StringBuilder();
            body.AppendLine("New 100 users are registered. On the website go to Manage/Users, find user by email, click elipsis menu and then Authorize User. Here is the list of new users:");
            emails.ForEach(e =>
                 body.AppendLine(e)
            );

            mail.Body = body.ToString();

            SendEmailBySmtp(mail);

        }

        public static void SendEmailContactAs(dtoContactUsData data)
        {
           
                MailMessage mail = new MailMessage();
           
                mail.From = new MailAddress(ConfigSettings.emailFrom, ConfigSettings.emailFromName);
                mail.To.Add(ConfigSettings.emailTo);

                mail.Subject = string.Format("New message from : {0}", data.Email);

                mail.Body = string.Format("New message from : {0} {1} {2} {3}", data.Email, data.FullName, data.PhoneNumber, data.Emirate);

                SendEmailBySmtp(mail);
                               
        }

        // use html template Confirm
        public static void SendEmailToApprovedUser(UserInfo ui)
        {
           
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(ConfigSettings.emailFrom, ConfigSettings.emailFromName);
                mail.To.Add(ui.Email);
                mail.IsBodyHtml = true;

                mail.Subject = string.Format("You are registered");

                string pwd = UserController.GeneratePassword();
                UserController.ChangePassword(ui, UserController.ResetPassword(ui, "").ToString(), pwd);

                string templatePath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, EmailTemplatesPath, "Confirm.html");
                string template = File.ReadAllText(templatePath);
                
                mail.Body = template
                    .Replace("{{Email Address}}", ui.Email)
                    .Replace("{{Password}}", pwd);

                SendEmailBySmtp(mail);

          
        }

        public static void SendEmail2FA(string email, string code)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(ConfigSettings.emailFrom, ConfigSettings.emailFromName);
            mail.To.Add(email);

            mail.Subject = string.Format("Verification code");

            mail.Body = string.Format("Your verification code: {0}", code);

            SendEmailBySmtp(mail);
        }

        // use html template Thanks
        public static void SendEmailToUserRegistration(string email, string firstName)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(ConfigSettings.emailFrom, ConfigSettings.emailFromName);
            mail.To.Add(email);
            mail.IsBodyHtml = true;

            mail.Subject = "Thank you for completing your registration";

            string templatePath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, EmailTemplatesPath, "Thanks.html");
            string template = File.ReadAllText(templatePath);

            mail.Body = template.Replace("{{first_name}}", firstName);

            SendEmailBySmtp(mail);
        }

    }
}