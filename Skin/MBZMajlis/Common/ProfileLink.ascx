<!-- USER PROFILE LINK-->
<% if (isLoggedIn){ %>
    <div class="user-bar-my-account" title="<%= GetString("GoToProfile") %>">
        <a href="/profile">
            <p class="display-name"><%=HttpContext.Current.User.Identity.Name%></p>
            <div class="avatar-block">
                <img class="avatar" src="<%=SkinPath%>assets/user-avatar.svg" alt="" />
            </div>
        </a>
    </div>
<% } %>
