<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>

<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />

<dnn:DnnCssInclude ID="scriptCss" runat="server" FilePath="style.css" PathNameAlias="SkinPath" Priority="100" />

<dnn:DnnJsInclude ID="scriptJs1" runat="server" FilePath="majilis.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude ID="scriptJs2" runat="server" FilePath="errorMessages.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude ID="scriptJs3" runat="server" FilePath="script.js" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude ID="lightZoomCss" runat="server" FilePath="light-zoom-style.css" PathNameAlias="SkinPath" Priority="100" />
<dnn:DnnJsInclude ID="lightzoomJs" runat="server" FilePath="lightzoom.js" PathNameAlias="SkinPath" />

<% var isLoggedIn = HttpContext.Current.User.Identity.IsAuthenticated; %>
<% Func<string, string> GetString = (s) => Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx"); %>
<% var isLobby = false; %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5WBLDHK');
</script>
