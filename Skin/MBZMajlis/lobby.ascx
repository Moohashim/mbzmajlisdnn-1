<%@ Control language="cs" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>

<!--#include file="Common/Common.ascx"-->
<% isLobby = true; %>
<div class="container">
<!--#include file="Common/ProfileLink.ascx"-->

<!--#include file="Common/Menu.ascx"-->
    <main class="maincontent">
        <section class="banner">
<% if (!isLoggedIn){ %>
			<div class="banner-bottom-button-group">
                <div class="registration-btn"><%= GetString("Registerhere") %></div>
                <div class="program-btn">
                    <a href="<%= GetString("ProgramLink") %>" target="_blank">
						<%= GetString("Program") %>
                    </a>
                </div>
            </div>
<% } %>
            <div class="banner-content">
                <div class="banner-content-top-text">
					<div class="banner-content-top-left-text">
						<p><%= GetString("MohamedBinZayed") %></p>
						<p><%= GetString("Majlisfor") %></p>
						<p><%= GetString("Futuregenerations") %></p>
                    </div>
                    <div class="banner-content-top-right-text">
                        <p><%= GetString("RegistrationNote") %></p>
                    </div>
                </div>
                <div class="banner-content-bottom-text">
                    <div class="banner-content-bottom-text-block">
                        <h1 class="banner-content-bottom-text-title">
                            <p><%= GetString("Thriving") %></p>
                            <p><%= GetString("inthe") %></p>
                            <p><%= GetString("nextnormal") %></p>
                        </h1>
                        <p class="banner-content-bottom-text-date"><%= GetString("Date") %></p>
                    </div>
                </div>
            </div>
            <div class="banner-image-block">
                <img alt="" src="<%=SkinPath%>assets/model1.png"/>
            </div>
        </section>
        <section class="lobby-about-section" id="about">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("About") %></p>
            </div>
            <div class="section-text-content">
                <div class="section-title">
                    <h2>
                        <%= GetString("MohamedBinZayed") %><br/>
                        <%= GetString("MajlisforFutureGenerations") %>
                    </h2>
                </div>
                <div id="ContentPane" runat="server" class="section-text"></div>
                    <div id="LoginPane" runat="server" style="display:none;"></div>
                    <div id="RegisterPane" runat="server" style="display:none;"></div>
                    <div id="ThankYouPane" runat="server" class="thankyouPart"></div>
		        <div class="open-modal">
		            <span id="open-modal"><%= GetString("Readmore") %></span>
		        </div>
            </div>
	        <div class="about-image-block">
                <img alt="" src="<%=SkinPath%>assets/model2.png"/>
            </div>
            <div class="small-bg"></div>
            
            <!-- Modal window -->
            <div class="modal-backdrop fade"></div>
            <div class="modal-window fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="btn-close"></button>
                        </div>
                        <div class="modal-body">
			                <div id="ReadMorePane" runat="server"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="bottom-btn-close">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- End Modal window -->
        </section>
        <section class="lobby-mission-section" id="mission-and-vision">
            <div class="small-bg"></div>
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("MissionVision") %></p>
            </div>
            <div class="section-text-content">
                <div class="section-title">
                    <h2><%= GetString("Mission") %></h2>
                </div>
                <div id="MissionPane" runat="server" class="section-text"></div>
            </div>
            <div class="section-text-content">
                <div class="section-title">
                    <h2><%= GetString("Vision") %></h2>
                </div>
                <div id="VisionPane" runat="server" class="section-text"></div>
            </div>
            <div class="mission-image-block">
                <img alt="" src="<%=SkinPath%>assets/model3.png"/>
            </div>
            <div class="section-text-content">
                <div class="section-title">
                    <h2><%= GetString("Purpose") %></h2>
                </div>
                <div id="PurposePane" runat="server" class="section-text"></div>
            </div>
            <div class="mission-image-block">
                <img alt="" src="<%=SkinPath%>assets/model3.png"/>
            </div>
        </section>
        <section class="lobby-speakers-section" id="speakers">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("Speakers") %></p>
            </div>
			<div class="section-title">
				<h2><%= GetString("KEYNOTESPEAKERS") %></h2>
			</div>
            <div id="AboveSpeakersPane" runat="server" class="above-speakers"></div>
            <div class="speakers-block">
                <ul class="speakers-nav-tab">
                    <li class="speakers-nav-tab-item active-nav tab-anchors" data-href="speakers-tab-1"><%= GetString("UAESPEAKERS") %></li>
                    <li class="speakers-nav-tab-item tab-anchors" data-href="speakers-tab-2"><%= GetString("INTERNATIONALSPEAKERS") %></li>
                </ul>

                <div class="active-tab" id="speakers-tab-1">
                    <div id="SpeakersPane" runat="server"></div>
                </div>
                <div id="speakers-tab-2">
                    <div id="Speakers2Pane" runat="server"></div>
                </div>

            </div>
        </section>
        <section class="lobby-venues-section" id="venues">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("Venues") %></p>
            </div>
            <div class="venueses-block">
                <div class="section-title">
                    <h2><%= GetString("Venues") %></h2>
                </div>
                <div id="VenuesPane" runat="server"></div>
            </div>
        </section>
        <section class="lobby-partners-section" id="partners">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("Partners") %></p>
            </div>
            <div class="partners-block">
                <div class="section-title">
                    <h2><%= GetString("Partners") %></h2>
                </div>

                <ul class="partners-nav-tab">
                    <li class="partners-nav-tab-item active-nav tab-anchors" data-href="tab-1"><%= GetString("InstitutionalPartners") %></li>
                    <li class="partners-nav-tab-item tab-anchors" data-href="tab-2"><%= GetString("SupportingPartners") %></li>
                </ul>
                <div class="active-tab" id="tab-1">
	                <div id="PartnersPane" runat="server"></div>
                </div>
                <div id="tab-2">
	                <div id="SupportingPartnersPane" runat="server"></div>
                </div>

            </div>
        </section>
        <section class="lobby-jubilee-lab-section" id="jubilee-lab">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("JubileeLab") %></p>
            </div>
            <div class="section-text-content">
                <div class="section-title">
                    <h2><%= GetString("JubileeLab") %></h2>
                </div>
                <div id="LabPane" runat="server" class="section-text"></div>
            </div>
            <div class="jubilee-image-block">
                <img alt="" src="<%=SkinPath%>assets/model4.png"/>
            </div>
        </section>
        <section class="lobby-contact-us-section" id="contact-us">
            <div class="section-header">
                <p class="section-header-nav-title"><%= GetString("Lobby") %></p>
                <p class="section-header-sub-nav--title">/<%= GetString("Contactus") %></p>
            </div>
            <div class="section-form-block">
                <div class="section-title">
                    <h2><%= GetString("Contactus") %></h2>
                </div>
               <div class="contact-form-container">
                    <div id="ContactUsPane" runat="server" class="contact-form"></div>
               </div>
            </div>
        </section>
    </main>
</div>
